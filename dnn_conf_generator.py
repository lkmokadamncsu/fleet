import random, copy
import models, tensorflow as tf
import keras.backend as K
import sys, csv, numpy as np
from keras.backend.tensorflow_backend import set_session
import pdb

run_meta = tf.RunMetadata()
keras_config = tf.ConfigProto()


def get_model_props(model_config):
    run_meta = tf.RunMetadata()
    with tf.Session(graph=tf.Graph(), config=keras_config) as sess:
        K.set_session(sess)
        net = models.get_model(model_config,profile_only=True)

        opts = tf.profiler.ProfileOptionBuilder.float_operation()
        flops = tf.profiler.profile(sess.graph, run_meta=run_meta, cmd='op', options=opts)

        flops = flops.total_float_ops

        shapes_mem_count = 0
        for l in net.layers:
            single_layer_mem = 1
            for s in l.output_shape:
                if s is None:
                    continue
                single_layer_mem *= s
            shapes_mem_count += single_layer_mem

        trainable_count = np.sum([K.count_params(p) for p in set(net.trainable_weights)])
        non_trainable_count = np.sum([K.count_params(p) for p in set(net.non_trainable_weights)])

        model_memory = 4.0 * (shapes_mem_count + 2*trainable_count + 2*non_trainable_count)

        # optimum_batch_size = int(((gpu_mem_in_gb * (1024 ** 3)) / model_memory))
        model_prop = copy.deepcopy(model_config)
        model_prop["shapes_mem_count"] = shapes_mem_count
        model_prop["trainable_weights"] = trainable_count
        model_prop["non_trainable_weights"] = non_trainable_count
        model_prop["memory"] = model_memory
        model_prop["flops"] = flops
        model_prop["params"] = net.count_params()

    return model_prop


def densenet_generator(num_of_nets, sum_of_nums):
    cluster_model_assignment_dict = {"model_name": "densenet",
                                     "batch_size": -1,
                                     "input_shape": (224, 224, 3)}

    def get_random_with_constant_sum(sum_of_nums, num_of_entries=4):
        print "densenet", cluster_model_assignment_dict["model_name"], sum_of_nums
        sum_of_nums = (sum_of_nums - 5) / 2
        # finds n random ints in [a,b] with sum of s
        hit = False
        nums = []
        while not hit:
            total, count = 0, 0
            nums = []
            while total < sum_of_nums and count < num_of_entries:
                r = random.randint(1, sum_of_nums)
                total += r
                count += 1
                nums.append(r)
            if total == sum_of_nums and count == num_of_entries and \
                    nums[0] < nums[1] and nums[2] > nums[3] and nums[0] < nums[3] and nums[1] < nums[2] and nums[
                0] > 4 and nums[0] < 15 and nums[3] > 9:
                hit = True
        # nums = [6, 12, 24, 16]

        cluster_model_assignment_dict["blocks"] = nums
        model_prop = get_model_flops(cluster_model_assignment_dict)
        model_prop["blocks"] = nums
        return tuple(nums), model_prop

    blocks_set = set()
    model_list = []
    for _ in range(num_of_nets):
        block_tuple, model_prop = get_random_with_constant_sum(sum_of_nums=sum_of_nums)
        if block_tuple not in blocks_set:
            model_list.append(model_prop)
        blocks_set.add(block_tuple)

    return sorted(model_list, key=lambda x: x["blocks"])




test_list = []
test_list.append([6, 12, 24, 16])
test_list.append([6, 12, 32, 32])
test_list.append([6, 12, 48, 32])
test_list.append([6, 12, 64, 48])
test_list += [t for t in test_list]

mode = sys.argv[1]
model = sys.argv[2]
if mode == "-a":
    file_path = sys.argv[3]

print "Started"
if mode == "-g":
    if model == "dense":
        print "generating densenet_generator"
        l = densenet_generator(num_of_nets=20, sum_of_nums=50)
        l = densenet_generator(num_of_nets=20, sum_of_nums=60)
        l = densenet_generator(num_of_nets=20, sum_of_nums=80)
        l = densenet_generator(num_of_nets=20, sum_of_nums=90)
        l = densenet_generator(num_of_nets=20, sum_of_nums=100)


        l += densenet_generator(num_of_nets=1, sum_of_nums=169)
        #l += densenet_generator(num_of_nets=1, sum_of_nums=201)
        #l += densenet_generator(num_of_nets=1, sum_of_nums=265)
        #l += densenet_generator(num_of_nets=1, sum_of_nums=182)
        for b in l:
            for t in b:
                print t, " ",
            print ""
    elif model == "resnet":
        print "generating resnet"
        l = resnet_generator(num_of_nets=1, sum_of_nums=50)
        l += resnet_generator(num_of_nets=1, sum_of_nums=101)
        l += resnet_generator(num_of_nets=1, sum_of_nums=152)
        l += resnet_generator(num_of_nets=1, sum_of_nums=128)
        l += resnet_generator(num_of_nets=1, sum_of_nums=182)
        for b in l:
            for t in b:
                print t, " ",
            print ""
    else:
        print "No model Support"

elif mode == "-a":
    if model == "dense":

        print "generating densenet_generator"
        cluster_model_assignment_dict = {"model_name": "densenet",
                                         "batch_size": -1,
                                         "input_shape": (224, 224, 3),
                                         "classes" : 1000
        }
        with open(file_path) as csvfile:
            l = csv.reader(x.replace('\0', '') for x in csvfile)
            print l
            header_added = False
            ans_string = "\n"
            for b in l:
                print b
                b = [int(t) for t in b]
                if len(b) == 0:
                    continue
                cluster_model_assignment_dict["blocks"] = b
                model_prop = get_model_props(cluster_model_assignment_dict)
                for t in b:
                    print t

                if not header_added:
                    print "\nlk ",
                    for k in sorted(model_prop.keys()):
                        print str(k) + " ; ",
                        ans_string += str(k) + " ; "
                    print ""
                    ans_string+="\n"
                    header_added = True
                print "\nlk ",
                for k in sorted(model_prop.keys()):
                    print str(model_prop[k])+" ; ",
                    ans_string += str(model_prop[k])+" ; "
                print ""
                ans_string += "\n"
            print ans_string
    elif model == "resnet":

        print "analyzing resnet"
        cluster_model_assignment_dict = {"model_name": "resnet",
                                         "batch_size": -1,
                                         "input_shape": (224, 224, 3),
                                         "classes" : 1000
        }
        with open(file_path) as csvfile:
            l = csv.reader(x.replace('\0', '') for x in csvfile)
            print l
            header_added = False
            ans_string = "\n"
            for b in l:
                print b
                b = [int(t) for t in b]
                if len(b) == 0:
                    continue
                cluster_model_assignment_dict["blocks"] = b
                model_prop = get_model_props(cluster_model_assignment_dict)
                for t in b:
                    print t

                if not header_added:
                    print "\nlk ",
                    for k in sorted(model_prop.keys()):
                        print str(k) + " ; ",
                        ans_string += str(k) + " ; "
                    print ""
                    ans_string += "\n"
                    header_added = True
                print "\nlk ",
                for k in sorted(model_prop.keys()):
                    print str(model_prop[k]) + " ; ",
                    ans_string += str(model_prop[k]) + " ; "
                print ""
                ans_string += "\n"
            print ans_string


    else:
        print "Error Not supported"
