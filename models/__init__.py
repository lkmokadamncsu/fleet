import keras.applications as keras_applications
import tensorflow as tf
import config
import numpy as np
import keras.backend as K

# print keras_applications.__version__

"""Not used


def vgg16(input_shape):
    return keras_applications.vgg16.VGG16(weights=None, include_top=True,  input_shape=input_shape)

def vgg19(input_shape):
    return keras_applications.vgg19.VGG19(weights=None, include_top=True,  input_shape=input_shape)


def nasnetlarge(input_shape):
    return keras_applications.nasnet.NASNetLarge(weights=None, include_top=True, input_shape=input_shape)
"""


def mobilenet(input_shape, width_multiplier, classes):
    return keras_applications.mobilenet.MobileNet(weights=None,
                                                  alpha=width_multiplier,
                                                  include_top=True,
                                                  input_shape=input_shape,
                                                  classes=classes
                                                  )


def resnet(input_shape, stack_args, classes, profile_only):
    if profile_only:
        return keras_applications.resnet.ResNet(stack_args=stack_args,
                                                weights=None,
                                                include_top=True,
                                                classes=classes,
                                                input_tensor=tf.placeholder('float32', shape=(1,) + input_shape))

    """if not input_shape:
        return keras_applications.resnet.ResNet(stack_args=stack_args,weights=None)"""

    return keras_applications.resnet.ResNet(stack_args=stack_args,
                                            weights=None,
                                            include_top=True,
                                            classes=classes,
                                            input_shape=input_shape)


def densenet(input_shape, blocks, classes, profile_only):
    if profile_only:
        return keras_applications.densenet.DenseNet(blocks=blocks,
                                                    weights=None,
                                                    include_top=True,
                                                    classes=classes,
                                                    input_tensor=tf.placeholder('float32', shape=(1,) + input_shape))

    return keras_applications.densenet.DenseNet(blocks=blocks,
                                                classes=classes,
                                                weights=None,
                                                include_top=True)


def get_model(args_dict, profile_only=False):
    basemodel = None
    model_name = args_dict["model_name"]
    input_shape = args_dict["input_shape"]
    classes = args_dict["classes"]

    if model_name == "resnet":
        stack_args = args_dict["blocks"]
        basemodel = resnet(input_shape=input_shape,
                           stack_args=stack_args,
                           classes=classes,
                           profile_only=profile_only)
    elif model_name == "densenet":
        blocks = args_dict["blocks"]
        basemodel = densenet(input_shape=input_shape,
                             blocks=blocks,
                             classes=classes,
                             profile_only=profile_only)
    elif model_name == "mobilenet":

        width_multiplier = args_dict["width_multiplier"]

        basemodel = mobilenet(input_shape, width_multiplier, classes)
    else:
        raise Exception(
            "NCSU PictureLab Exception: model not supported : " + model_name)
    return basemodel





def get_model_flops_and_memory_usage(gpu_mem_in_gb, model):

    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p)
                              for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p)
                                  for p in set(model.non_trainable_weights)])

    model_memory = 4.0*(shapes_mem_count + 2 *
                        trainable_count + 2*non_trainable_count)

    optimum_batch_size = int(((gpu_mem_in_gb*(1024**3))/model_memory))

    config.state.state.log_dict["Model : trainable_count"] = trainable_count
    config.state.state.log_dict["Model : non_trainable_count"] = non_trainable_count
    config.state.state.log_dict["Model : model_memory"] = model_memory
    print flops.total_float_ops, optimum_batch_size
    return flops.total_float_ops, optimum_batch_size
