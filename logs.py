import time
import psutil
import time
import subprocess
import sys
import os
#from multiprocessing import Process
from threading import Thread
import config
import logging

logging.basicConfig(level=logging.INFO)

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
world_size = comm.Get_size()

class Resource_Util_Logger(Thread):
    def __init__(self, delay):
        super(Resource_Util_Logger, self).__init__()

        self.stopped = False
        self.delay = delay  # Time between calls to GPUtil
        self.daemon = True
        self.start()

    def run(self):
        while not self.stopped:
            gpu_util = ""
            """
            gpu_util = subprocess.check_output(
                ["nvidia-smi", "--query-gpu=utilization.gpu,utilization.memory", "--format=csv"])
            gpu_util = ";".join(gpu_util.split('\n')[1:])
            gpu_util = "lk gpu "+gpu_util
            """
            cpu_usage = ""
            cpu_usage = psutil.cpu_percent(interval=0, percpu=True)

            cpu_time_percent = ""
            #cpu_time_percent = psutil.cpu_times_percent(interval=0, percpu=True)

            preprocessing_queue_len = None
            trainer_queue_len = None
            if config.state.state:
                if config.state.state and hasattr(config.state.state,
                                                  'preprocessor_mpi_queue'):
                    preprocessing_queue_len = config.state.state.preprocessor_mpi_queue.qsize()

                trainer_queue_len = config.state.state.trainer_mpi_queue.qsize()

            log_str = "\n lk~util_logs~rank->{}___time->{}___pl:{}___tl:{}___cpu:{}\n"
            log_str = log_str.format(rank,
                                     time.time(),
                                     preprocessing_queue_len,
                                     trainer_queue_len,
                                     cpu_usage)

            logging.error(log_str)
            time.sleep(self.delay)

    def stop(self):
        self.stopped = True


if __name__ == "__main__":
    m = Resource_Util_Logger(1)
    time.sleep(5)
    m.stop()
