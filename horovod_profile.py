from __future__ import print_function
import keras, time
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
import math
import tensorflow as tf
import horovod.keras as hvd



import keras
import csv
import time
import pdb
import keras.backend as K
from keras.backend.tensorflow_backend import set_session
import argparse
import models
import tensorflow as tf
from slim.producer_preprocessing import vgg_preprocessing
from keras.callbacks import EarlyStopping


from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
world_size = comm.Get_size()

import time

class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)

class State:
    datapath = ""
    no_of_epochs = 1
    training_samples = 30606
    no_of_cores = 16
    labels = 1000
    model_config = ""

state = State()


def preprocessor_fn(filename, label):
    image_string = tf.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = vgg_preprocessing.preprocess_for_train(image, 224, 224)
    label = tf.one_hot(label, state.labels)
    return image, label


def file_path_and_label_function():
    images = []
    labels = []
    f = open(state.datapath+'/labels.csv')
    csv_f = csv.reader(f)
    next(csv_f, None)
    for row in csv_f:
        for _ in range(1):
            images.append((state.datapath+"/"+row[0]).strip())
            labels.append(int(row[1].strip()))
    images_epochs = []
    labels_epochs = []
    for _ in range(1):
        images_epochs.extend(images)
        labels_epochs.extend(labels)

    print("Images", len(images_epochs), " : Labels", len(labels_epochs))
    return images_epochs, labels_epochs


def data_generator():
    names, labels = file_path_and_label_function()

    tf_filenames = tf.constant(names)

    tf_labels = tf.constant(labels)

    dataset = tf.data.Dataset.from_tensor_slices((tf_filenames, tf_labels))

    # num_of_parallel_calls
    dataset = dataset.map(map_func=preprocessor_fn,
                          num_parallel_calls=state.no_of_cores)
    #dataset = dataset.prefetch(tf.contrib.data.AUTOTUNE)
    return dataset


def get_model(model_type, blocks, batch_size):
    global state
    if model_type == "D":
        model_type = "densenet"
    elif model_type == "R":
        model_type = "resnet"

    model_args = {"model_name": model_type,
                  "batch_size": batch_size,
                  "classes": state.labels,
                  "blocks": blocks,
                  "input_shape": (224, 224, 3)}


    state.model_config = model_args
    basemodel = models.get_model(model_args)
    return basemodel


def preprocess_data_generator(batch_size):
    dataset = data_generator()
    dataset = dataset.repeat(state.no_of_epochs)
    dataset = dataset.batch(batch_size)
    dataset.make_one_shot_iterator()
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()

    tensorflow_config = tf.ConfigProto(device_count={"GPU": 0},
                                       inter_op_parallelism_threads=state.no_of_cores,
                                       allow_soft_placement=True,
                                       log_device_placement=True,
                                       intra_op_parallelism_threads=state.no_of_cores
                                       )
    sess = tf.Session(config=tensorflow_config)
    while True:
        image_tuple = sess.run(next_element)
        yield image_tuple[0], image_tuple[1]
    sess.close()

def train(model_type, blocks, gpu_mem, flops, memory, params):

    hvd.init()

    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = str(0)
    sess = tf.Session(config=config)
    K.set_session(sess)

    batch_size = (gpu_mem*1024*1024*1024)/memory
    # Horovod: adjust number of epochs based on number of GPUs.

    model = get_model(model_type, blocks, batch_size)

    opt = keras.optimizers.Adadelta(1.0 * hvd.size())

    # Horovod: add Horovod Distributed Optimizer.
    opt = hvd.DistributedOptimizer(opt)


    init = K.tf.global_variables_initializer()
    K.get_session().run(init)
    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=opt,
                  metrics=['accuracy'])
    time_callback = TimeHistory()
    callbacks = [
        # Horovod: broadcast initial variable states from rank 0 to all other processes.
        # This is necessary to ensure consistent initialization of all workers when
        # training is started with random weights or restored from a checkpoint.
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
        time_callback,
    ]
    gen = preprocess_data_generator(batch_size)
    steps_per_epoch = state.training_samples/(hvd.size()*batch_size)
    start_time = time.time()
    hist = model.fit_generator(generator=gen,
                               steps_per_epoch=10,
                               callbacks=callbacks,
                               epochs=3,
                               verbose=1)
    end_time = time.time()

    print("batchsize is {}".format(batch_size))
    print("lk~{}___{}___{}___{}___{}___{}".format(hvd.size(),
                                     end_time - start_time,
                                     steps_per_epoch,
                                     batch_size,
                                     time_callback.times,
                                     state.model_config
    ))
    sess.close()
    print("barrier")
    comm.barrier()
    return hist, end_time-start_time, batch_size


def main():

    parser = argparse.ArgumentParser(description='help for command')
    parser.add_argument('--model_args_csv_file', type=str,
                        help='no of processes on each node', required=True)
    parser.add_argument('--datapath', type=str,
                        help='path to dataset folder', required=True)
    parser.add_argument('--gpu_mem', type=int, required=True)
    parser.add_argument('--index_from', type=int, required=True)
    parser.add_argument('--index_to', type=int, required=True)

    args = parser.parse_args()
    state.datapath = args.datapath


    with open(args.model_args_csv_file, 'rb') as f:
        lines = f.readlines()
        data = [l.split(',') for l in lines[1:]]
        print(len(data))
    assigned_model = []
    gpu_mem = args.gpu_mem

    for i in range(len(data)):
        assigned_model.append((data[i][0],
                               [int(t) for t in data[i][1:5]],
                               int(data[i][6]),
                               int(data[i][7]),
                               int(data[i][8])
                               ))
    for index in range(args.index_from,args.index_to):
        model_type, blocks, flops, memory, params = assigned_model[index]
        start = time.time()
        hist, training_time, batch_size = train(
            model_type, blocks, gpu_mem, flops, memory, params)
        end = time.time()
        # pdb.set_trace()
        log_str = "\n lk~{}___{}___{}___{}___{} \n".format([model_type, blocks, flops, memory, params],training_time,
                                                                 batch_size,
                                                                 hist.history,
                                                                 state
                                                                 )



main()
