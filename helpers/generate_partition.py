import horovod.keras as hvd
import mpi4py.rc, collections
print(mpi4py.rc.thread_level)
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

import argparse, threading
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import keras.backend as K
import models, keras, time,numpy as np
from keras.models import Model
import logging as logger

hvd.init()

log_dict = dict()
class Generator_State:
    batch_size = 10
    input_size = None
    model_name = None
    epochs = None
    flops = None
    local_rank = None
    gpu_mem_in_gb = 11
    log_dict = dict()

generator_state = Generator_State()

def data_gen_1():
    print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
    batch_size = generator_state.batch_size
    input_size = generator_state.input_size

    batch_index = 0
    while True:
    #for _ in xrange(generator_state.epochs):
        image_ = np.zeros((224,224,3))
        label_ = keras.utils.to_categorical(6, num_classes=1000)
        image_ = np.reshape(image_, newshape=(1, 224, 224, 3))
        label_ = np.reshape(label_, newshape=(1, 1000))

        if batch_index < batch_size:
            if batch_index == 0:
                image = image_
                label = label_
            else:
                image = np.append(image, image_, axis=0)
                label = np.append(label, label_, axis=0)
            batch_index += 1
            continue
        else:
            batch_index = 0
            yield image, label



def dist_vgg():

    keras_config = tf.ConfigProto()
    keras_config.gpu_options.visible_device_list = str(generator_state.local_rank)
    keras_config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    #keras_config.log_device_placement = True  # to log device placement (on which device the operation ran)
    sess = tf.Session(config=keras_config)
    set_session(sess)  # set this TensorFlow session as the default session for Keras

    basemodel = models.get_model(generator_state.model_name,generator_state.input_size)


    last = basemodel.output

    model = Model(basemodel.input, last)
    #opt = keras.optimizers.Adadelta(1.0)

    #model = multi_gpu_model(model, gpus=2)

    # Horovod: adjust learning rate based on number of GPUs.
    opt = keras.optimizers.Adadelta(1.0 * hvd.size())

    # Horovod: add Horovod Distributed Optimizer.
    opt = hvd.DistributedOptimizer(opt)

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=opt,
                  metrics=['accuracy'])

    callbacks = [
        # Horovod: broadcast initial variable states from rank 0 to all other processes.
        # This is necessary to ensure consistent initialization of all workers when
        # training is started with random weights or restored from a checkpoint.
        hvd.callbacks.BroadcastGlobalVariablesCallback(0)
    ]

    nb_train_samples = 1000
    batch_size = 100
    generator = data_gen_1()

    starttime = time.time()
    print "Starting >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    try:
        model.fit_generator(
            generator,
            callbacks=callbacks,
            steps_per_epoch=(nb_train_samples // batch_size),
            epochs=1,
            verbose=1,
            # use_multiprocessing=True
        )
    except StopIteration:
        logger.info("Stopped iteration before receiveing all data. This may be due to incorrect number of data sample")
    finally:
        logger.info("Stopped Training")
    endtime = time.time()
    generator_state.log_dict["TRAINING_EXEC_TIME"] = endtime - starttime
    generator_state.log_dict["DATA_CONSUMED"] = 100*generator_state.epochs*generator_state.batch_size
    generator_state.log_dict["MODEL"] = generator_state.model_name
    #generator_state.log_dict["NO_OF_GPUS"] = hvd.size()




def get_model_flops_and_memory_usage(model):
    run_meta = tf.RunMetadata()
    float_operations = tf.profiler.ProfileOptionBuilder.float_operation()
    flops = tf.profiler.profile(graph=K.get_session().graph,
                                run_meta=run_meta, cmd='op', options=float_operations)

    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

    model_memory  = 4.0*(shapes_mem_count + 2*trainable_count + 2*non_trainable_count)

    optimum_batch_size = int((generator_state.gpu_mem_in_gb*(1024**3))/model_memory)

    return flops.total_float_ops, optimum_batch_size

def main():
    parser = argparse.ArgumentParser(description='help for command')

    parser.add_argument('--no_of_gpus_per_node', type=int, help=' Number of GPUs per node', required=True)
    parser.add_argument('--model_name', type=str, help='model_name', required=True)
    parser.add_argument('--epochs', type=int, help='epochs', required=True)

    args = parser.parse_args()

    # import architecture file
    print hvd.rank(), rank
    generator_state.local_rank = rank % args.no_of_gpus_per_node
    generator_state.model_name = args.model_name
    generator_state.input_size = (224,224,3)
    generator_state.epochs = args.epochs
    dist_vgg()



if __name__ == "__main__":
    main()
    time.sleep(2)
    for t in threading.enumerate()[1:]:
        print rank, t, "\n\n\n\n"
    print "Should Exit Now"