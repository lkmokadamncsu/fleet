
import sys

import glob, os

label_dict_name_num = {}
label_dict_num_name = {}
counter={}
i = 0
for file in os.listdir(sys.argv[1]+"/"):
    label_dict_name_num[file]=i
    label_dict_num_name[i]=file
    counter[i]=0
    i+=1
print len(label_dict_name_num.keys())

for file in glob.glob(sys.argv[1]+"/*/"+"*.JPEG"):
    label = label_dict_name_num[file.split("/")[-1].split("_")[0]]
    counter[label]+=1
label_dict = {}
top_1000 =  sorted(counter.items(), key = lambda x:x[1], reverse=True)
print top_1000[1000:]
top_1000 = top_1000[:1000]

################################################################################################
new_label = 0
for c in top_1000:
    label_dict[label_dict_num_name[c[0]]] = new_label
    new_label+=1
print new_label==1000
assert(new_label==1000)
files = []
error = set()
with open('labels.csv','w') as f:
    print "Inside"
    for file in glob.glob(sys.argv[1]+"/*/"+"*.JPEG"):
        try:
            label = label_dict[file.split("/")[-1].split("_")[0]]
            f.write(file + " , " + str(label) + "\n")
        except KeyError:
            error.add("*****"+file.split("/")[-1].split("_")[0])
print len(top_1000)
print error, len(error)