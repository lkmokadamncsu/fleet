import random, math
sample_points = [1,2,3,4,8,12,16,24,28,32]

init = [32,52,56,120,32,16]
speedup = [1.8,1.7,1.46,1.54,1.63,1.94]

last = [0 for _ in range(len(speedup))]
for n_gpu in sample_points:
    for dnn_i in range(len(init)):
        val = init[dnn_i] * speedup[dnn_i] * n_gpu - random.random()*(2**math.log(init[dnn_i] * speedup[dnn_i] * n_gpu))
        print("%s %s %s"%(dnn_i , n_gpu , val ))

