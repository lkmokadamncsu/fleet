from mpi4py import MPI
comm = MPI.COMM_WORLD


import tensorflow as tf
import random
import config.state
import Queue
import threading, sys, os, time
import numpy as np


def fill_input_queue():
    while not config.state.state.shutdown:
        data_list = np.empty((224*224*3+1), dtype=np.float32)
        config.state.state.preprocessor_mpi_input_queue.put(data_list)



def generate_dataset(preprocessing_function, file_path_and_label_function, no_of_preprocessing_ranks):
    names, labels = file_path_and_label_function()

    no_of_images = len(names)

    assigned_filenames = []
    assigned_labels = []

    # TODO change [1,3,5],[2,4,6] to [1,2,3],[4,5,6]
    for i in range(config.state.state.rank, no_of_images, no_of_preprocessing_ranks):
        assigned_filenames.append(names[i])
        assigned_labels.append(labels[i])

    combined_tuple = list(zip(assigned_filenames, assigned_labels))
    random.shuffle(combined_tuple)

    assigned_filenames[:], assigned_labels[:] = zip(*combined_tuple)

    tf_filenames = tf.constant(assigned_filenames)

    tf_labels = tf.constant(assigned_labels)

    dataset = tf.data.Dataset.from_tensor_slices((tf_filenames, tf_labels))

    # num_of_parallel_calls
    dataset = dataset.map(map_func = preprocessing_function, num_parallel_calls = config.state.state.no_of_parallel_preprocessing_calls)

    return dataset, len(assigned_labels), len(assigned_labels)


def data_producer(preprocessing_function,
                  file_path_and_label_function, state):



    total_no_of_preprocessors = len(config.state.state.preprocessing_ranks_list)

    # generated dataset and created graph for iterating next element
    dataset, l1, l2 = generate_dataset(preprocessing_function,
                               file_path_and_label_function,
                               total_no_of_preprocessors)
    dataset = dataset.repeat(config.state.state.epochs)
    config.state.state.logger.critical("lkdebug~epoch_count  %d", config.state.state.epochs)
    iterator = dataset.make_one_shot_iterator()

    next_element = iterator.get_next()

    tensorflow_config = tf.ConfigProto(
        device_count={"GPU": 0},
        inter_op_parallelism_threads = 20,  #config.state.state.no_of_parallel_preprocessing_calls,
        allow_soft_placement=True,
        #log_device_placement=True,
        intra_op_parallelism_threads = 20,  # config.state.state.no_of_parallel_preprocessing_calls,
    )

    with tf.Session(config=tensorflow_config) as sess:

        ttt = 0
        img_count = 0
        while (True):
            w = 0
            cur_batch = []
            try:
                for _ in xrange(config.state.state.preprocessing_data_size):
                    img_count += 1
                    image, label = sess.run(next_element)
                    data_packet = config.state.state.preprocessor_mpi_input_queue.get()
                    data_packet_val = np.append(image.flatten(), np.array(label, dtype=np.float32))
                    np.copyto(data_packet, data_packet_val)
                    config.state.state.preprocessor_mpi_queue.put(data_packet)
                    config.state.state.logger.debug("added data tuple inside mpi_queue %d" % config.state.state.rank)
                    #config.state.state.logger.critical("img_count {} {} {} {} ".format(config.state.state.rank,img_count,config.state.state.trainer_mpi_queue.qsize(),l2))
            except Exception as ex:

                if ex.__class__.__name__ == "OutOfRangeError":
                    config.state.state.logger.critical("OutOfRangeError : All data consumed by dataset %d : data produced %d" % (config.state.state.rank, img_count))
                    break

                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(ex.__class__.__name__, ex.args)
                config.state.state.logger.critical("Warning : " + message + " " + str(w))
                w += 1
            ttt += 1
            #config.state.state.logger.critical("img_count {} {} {} ".format(config.state.state.rank,img_count,ttt))
    config.state.state.preprocessor_mpi_queue.put(np.full((224*224*3+1), -1, dtype=np.float32))
    config.state.state.logger.info("done with data_producer %d" % config.state.state.rank)
    sess.close()
