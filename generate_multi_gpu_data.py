from __future__ import print_function

import argparse
import logging
import time

import horovod.keras as hvd
import numpy as np
import tensorflow as tf
from mpi4py import MPI

import keras
import models
from keras import backend as K

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
world_size = comm.Get_size()
logging.basicConfig(level=logging.DEBUG)
logging.debug("script loaded ____ rank : {} ____ time : {}".format(
    rank, time.time()))


class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)


class State:

    max_number_of_ranks_per_model = 4
    no_of_epochs = 1
    training_samples = 30606
    no_of_cores = 16
    labels = 1000
    model_config = ""
    steps = -1


state = State()


def get_model(model_type, blocks, batch_size):
    global state
    if model_type == "D":
        model_type = "densenet"
    elif model_type == "R":
        model_type = "resnet"

    model_args = {
        "model_name": model_type,
        "batch_size": batch_size,
        "classes": state.labels,
        "blocks": blocks,
        "input_shape": (224, 224, 3)
    }

    state.model_config = model_args

    basemodel = models.get_model(model_args)
    return basemodel


def preprocess_data_generator(batch_size):

    while True:
        images = np.random.randint(
            low=0, high=255, size=(batch_size, 224, 224, 3))

        labels = np.random.randint(low=0, high=state.labels, size=batch_size)

        labels = keras.utils.to_categorical(
            labels, num_classes=1000, dtype='float32')

        yield images, labels


def train(ranks_assigned, model_type, blocks, gpu_mem, flops, memory, params):
    start_time = time.time()

    # Horovod: pin GPU to be used to process local rank (one GPU per process)
    config = tf.ConfigProto(
        intra_op_parallelism_threads=20, inter_op_parallelism_threads=20)
    config.gpu_options.allow_growth = True
    #config.gpu_options.visible_device_list = str(rank)
    sess = tf.Session(config=config)
    K.set_session(sess)

    batch_size = (gpu_mem * 1024 * 1024 * 1024) / memory
    # Horovod: adjust number of epochs based on number of GPUs.

    model = get_model(model_type, blocks, batch_size)

    opt = keras.optimizers.Adadelta(1.0 * hvd.size())

    # Horovod: add Horovod Distributed Optimizer.
    opt = hvd.DistributedOptimizer(opt)

    init = K.tf.global_variables_initializer()
    K.get_session().run(init)

    logging.debug("compile started ____ rank : {} ____ time : {}".format(
        rank, time.time()))
    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer=opt,
        metrics=['accuracy'])
    logging.debug("compile ended ____ rank : {} ____ time : {}".format(
        rank, time.time()))

    time_callback = TimeHistory()
    callbacks = [
        # Horovod: broadcast initial variable states from rank 0 to all other processes.
        # This is necessary to ensure consistent initialization of all workers when
        # training is started with random weights or restored from a checkpoint.
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),
        time_callback,
    ]

    logging.debug("generator started ____ rank : {} ____ time : {}".format(
        rank, time.time()))
    gen = preprocess_data_generator(batch_size)
    logging.debug("generator ended ____ rank : {} ____ time : {}".format(
        rank, time.time()))

    logging.debug("training started ____ rank : {} ____ time : {}".format(
        rank, time.time()))
    start_time = time.time()
    hist = model.fit_generator(
        generator=gen,
        steps_per_epoch=state.steps,
        callbacks=callbacks,
        epochs=4,
        verbose=0)
    end_time = time.time()
    logging.debug("training ended ____ rank : {} ____ time : {}".format(
        rank, time.time()))
    sess.close()
    keras.backend.clear_session()
    return hvd.rank(), hist, end_time - start_time, batch_size, time_callback.times


def get_comm_ranks_list_model_wise():

    max = state.max_number_of_ranks_per_model

    groups = []

    groups.append(range(0, max))

    cur_rank = max

    for i in range(1, max / 2):

        g = [cur_rank + j for j in range(0, i)]
        groups.append(g)
        cur_rank = g[-1] + 1

        g = [cur_rank + j for j in range(0, max - i)]
        groups.append(g)
        cur_rank = g[-1] + 1

    groups.append(range(cur_rank, cur_rank + max / 2))

    for g in groups:
        if rank in g:
            return g
    return None


def get_comm_ranks_list_rank_wise(number_of_ranks):

    if number_of_ranks == 1:
        assignment_step = 1
    elif number_of_ranks == 2:
        assignment_step = 2
    else:
        assignment_step = ((number_of_ranks + 3) / 4) * 4

    models_in_stage = int(world_size / assignment_step)

    stage_member_index = 0
    for i in range(0, models_in_stage * assignment_step, assignment_step):

        if (i + number_of_ranks) > world_size:
            break

        if rank in range(i, i + number_of_ranks):
            return True, range(
                i, i + number_of_ranks), models_in_stage, stage_member_index

        if rank in range(i + number_of_ranks, i + assignment_step):
            return True, range(
                i + number_of_ranks,
                i + assignment_step), models_in_stage, stage_member_index

        stage_member_index += 1

    return False, [rank], models_in_stage, 0

def filter_data(lines, nor):
    new_lines = []
    filter_list = range(len(lines))
    if nor == 9:
        filter_list = [51, 64, 77, 90]
    elif nor == 10:
        filter_list = [10, 11, 23, 24, 36, 37, 49, 50, 62, 63, 75, 76, 88, 89]
    elif nor == 11:
        filter_list = [39, 47, 52, 60, 65, 73, 78, 86, 91, 99]
    elif nor == 12:
        filter_list = [26, 27, 31, 36, 39, 40, 41, 43, 44, 46, 47, 49, 52, 53, 54, 56, 57, 59, 60, 62, 65, 66, 67, 69, 70, 72, 73, 75, 78, 79, 80, 82, 83, 85, 86, 87, 88, 91, 92, 93, 95, 96, 98, 99]
    elif nor == 13:
        filter_list = [18, 28, 32, 38, 42, 48, 52, 58, 62, 68, 72, 78, 79, 82, 85, 88, 89, 92, 94, 95, 98, 99]
    elif nor == 14:
        filter_list = [1, 11, 21, 23, 28, 31, 33, 38, 40, 41, 43, 48, 50, 51, 53, 58, 60, 61, 63, 68, 70, 71, 73, 78, 80, 81, 83, 88, 90, 91, 93, 98]
    elif nor == 15:
        filter_list = [20, 30, 40, 50, 60, 61, 70, 71, 80, 81, 90, 91]
    elif nor == 16:
        filter_list = [18, 22, 25, 28, 30, 32, 35, 38, 40, 42, 45, 48, 50, 52, 55, 58, 60, 62, 65, 68, 70, 72, 75, 78, 80, 82, 85, 88, 90, 92, 95, 98]
    elif nor > 16:
        filter_list = [0, 2, 3, 4, 5, 6, 7, 8]
    for i, l in enumerate(lines):
        if i in filter_list:
            new_lines.append(l)
    return new_lines

def main_trainer(model_args_csv_file, gpu_mem, index_from, index_to, steps,
                 number_of_ranks):

    global state

    state.steps = steps
    logging.debug("reading started ____ rank : {} ____ time : {}".format(
        rank, time.time()))

    with open(model_args_csv_file, 'rb') as f:
        lines = f.readlines()
        lines = filter_data(lines[1:], number_of_ranks)
        data = [l.split(',') for l in lines]
        print(lines)

    logging.debug("reading ended ____ rank : {} ____ time : {}".format(
        rank, time.time()))

    models_list = []

    for i in range(len(data)):
        models_list.append((data[i][0], [int(t) for t in data[i][1:5]],
                            int(data[i][6]), int(data[i][7]), int(data[i][8])))

    do_log, ranks_assigned, sum_of_models_per_stage, stage_member_index = get_comm_ranks_list_rank_wise(
        number_of_ranks)

    log_str = "lkdebug1,rank = {}, ranks_assigned = {}, stage_member_index = {}, do_log = {}"

    log_str = log_str.format(rank, "_".join(
        [str(temp) for temp in ranks_assigned]), stage_member_index, do_log)
    logging.info(log_str)

    logging.debug("hvd init started ____ rank : {} ____ time : {}".format(
        rank, time.time()))

    hvd.init(ranks_assigned)

    logging.debug("hvd init ended ____ rank : {} ____ time : {}".format(
        rank, time.time()))

    for stage_index in range((index_to / sum_of_models_per_stage) + 1):
        logging.debug("loop started ____ rank : {} ____ time : {}".format(
            rank, time.time()))
        model_id = stage_index * sum_of_models_per_stage + \
            stage_member_index + index_from

        log_str = "lkdebugin do_log = {}, rank = {}, model_id = {}, stage_index = {}, sum_of_models_per_stage = {}, stage_member_index = {}, ranks_assigned = {}"

        log_str = log_str.format(
            do_log, rank, model_id, stage_index, sum_of_models_per_stage,
            stage_member_index, "_".join(
                [str(temp) for temp in ranks_assigned]))
        logging.info(log_str)

        if (not do_log) \
           or (model_id >= index_to) \
           or (model_id >= len(models_list)):
            model_id = 0
            do_log = 0

        model_type, blocks, flops, memory, params = models_list[model_id]
        start = time.time()
        cluster_rank , hist, training_time, batch_size, times = train(
            ranks_assigned, model_type, blocks, gpu_mem, flops, memory, params)
        end = time.time()

        # pdb.set_trace()
        if do_log and cluster_rank == 0:

            log_str = "\n\n lk~{}___{}___{}___{}___{}___{}___{}___{}___{} \n\n"
            log_str = log_str.format(
                [model_type, blocks, flops, memory, params], training_time,
                batch_size, hist.history, state.__dict__, times, end - start,
                model_id, hvd.size())

            logging.critical(log_str)
    hvd.shutdown()


def main():
    parser = argparse.ArgumentParser(description='help for command')
    parser.add_argument(
        '--model_args_csv_file',
        type=str,
        help='no of processes on each node',
        required=True)
    parser.add_argument('--gpu_mem', type=int, required=True)
    parser.add_argument('--index_from', type=int, required=True)
    parser.add_argument('--index_to', type=int, required=True)
    parser.add_argument('--steps', type=int, required=True)
    parser.add_argument('--number_of_ranks', type=int, required=True)

    args = parser.parse_args()

    main_trainer(
        model_args_csv_file=args.model_args_csv_file,
        gpu_mem=args.gpu_mem,
        index_from=args.index_from,
        index_to=args.index_to,
        steps=args.steps,
        number_of_ranks=args.number_of_ranks)


# main function starts here
main()
