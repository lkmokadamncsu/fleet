import sys, collections
import numpy as np
import matplotlib.pyplot as plt

def get_table(file):
    table = collections.defaultdict(dict)
    with open(file) as f:
        lines = f.readlines()
        for l in lines:
            dnn_id = int(l.split()[0])
            n_gpu = int(l.split()[1])
            val = float(l.split()[2])
            table[dnn_id][n_gpu] = val

    print table
    return table


def get_function(table, dnn_id):
    x = table[dnn_id].keys()
    y = table[dnn_id].values()
    z = np.polyfit(x, y, 5)
    f = np.poly1d(z)
    return f

def get_all_dnn_curve(table):
    func_l = []
    for dnn_id in sorted(table.keys()):
        f = get_function(table,dnn_id)
        func_l.append(f)
    return func_l


def display_data(f,max_gpu):


    # calculate new x's and y's
    x_new = np.linspace(1, max_gpu, 50)
    y_new = f(x_new)

    plt.plot(x_new, y_new, '', x_new, y_new)
    plt.plot()
    plt.xlim([1 - 1, max_gpu + 1])
    plt.show()

def main():
    file = sys.argv[1]
    table = get_table(file)
    func_l = get_all_dnn_curve(table)

    for f in func_l:
        display_data(f,32)


if __name__ == '__main__':
    main()
