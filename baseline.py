import keras,logs,os
import time, csv
import pdb
import keras.backend as K
from keras.backend.tensorflow_backend import set_session
import argparse
import models
import tensorflow as tf
from slim.producer_preprocessing import vgg_preprocessing
from keras.callbacks import EarlyStopping
import logging

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
world_size = comm.Get_size()


import time

class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)

class State:
    datapath = ""
    no_of_epochs = 2
    training_samples = 30607
    no_of_cores = 20
    labels = 258


state = State()


def preprocessor_fn(filename, label):
    image_string = tf.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = vgg_preprocessing.preprocess_for_train(image, 224, 224)
    label = tf.one_hot(label, 1000)
    return image, label


def file_path_and_label_function():
    images = []
    labels = []
    f = open(state.datapath+'/labels.csv')
    csv_f = csv.reader(f)
    next(csv_f, None)
    for row in csv_f:
        for _ in range(1):
            images.append((state.datapath+"/"+row[0]).strip())
            labels.append(int(row[1].strip()))
    images_epochs = []
    labels_epochs = []
    for _ in range(1):
        images_epochs.extend(images)
        labels_epochs.extend(labels)

    print("Images", len(images_epochs), " : Labels", len(labels_epochs))
    return images_epochs, labels_epochs


def data_generator():
    names, labels = file_path_and_label_function()

    tf_filenames = tf.constant(names)

    tf_labels = tf.constant(labels)

    dataset = tf.data.Dataset.from_tensor_slices((tf_filenames, tf_labels))

    # num_of_parallel_calls
    dataset = dataset.map(map_func=preprocessor_fn,
                          num_parallel_calls=8)
    #dataset = dataset.prefetch(tf.contrib.data.AUTOTUNE)
    return dataset


def get_model(model_type, blocks, batch_size):
    if model_type == "D":
        model_type = "densenet"
    elif model_type == "R":
        model_type = "resnet"

    model_args = {"model_name": model_type,
                  "batch_size": batch_size,
                  "classes": 1000,
                  "blocks": blocks,
                  "input_shape": (224, 224, 3)}
    basemodel = models.get_model(model_args)
    return basemodel


def preprocess_data_generator(batch_size):
    dataset = data_generator()
    dataset = dataset.repeat(state.no_of_epochs)
    dataset = dataset.batch(batch_size)
    #dataset.make_one_shot_iterator()
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()

    tensorflow_config = tf.ConfigProto(
                                       device_count={"GPU": 0},
                                       inter_op_parallelism_threads=state.no_of_cores,
                                       log_device_placement=True,
                                       intra_op_parallelism_threads=state.no_of_cores
                                       )

    with tf.Session(config=tensorflow_config) as sess:

        while True:
            image_tuple = sess.run(next_element)
            yield image_tuple[0], image_tuple[1]


def train(model_type, blocks, gpu_mem, flops, memory, params, system_type, predict_batchsize, model_id):
    keras_config = tf.ConfigProto()

    print rank
    if system_type == 'lab':
        keras_config.gpu_options.visible_device_list = str(rank)
    elif system_type == 'summitdev':
        keras_config.gpu_options.visible_device_list = str(rank%4)
    elif system_type == "summit":
        keras_config.gpu_options.visible_device_list = str(rank%6)
    elif system_type == "summitdev_all_gpu_0":
        keras_config.gpu_options.visible_device_list = str(0)
    else:
        raise("PictLab Error:  Unsupported System")
    print "lk >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",rank, keras_config.gpu_options.visible_device_list

    # dynamically grow the memory used on the GPU
    #keras_config.gpu_options.allow_growth = True
    # keras_config.log_device_placement = True  # to log device placement (on which device the operation ran)
    if predict_batchsize:
        batch_size = (gpu_mem*(1024**3))/memory
    else:
        batch_size = 8
    sess = tf.Session(config=keras_config)
    set_session(sess)
    model = get_model(model_type, blocks, batch_size)
    opt = keras.optimizers.Adadelta(1.0)
    #early_stop = EarlyStopping(monitor='loss', patience=10)

    resume_from_epoch = 0
    """checkpoint_path = None
    for try_epoch in range(state.no_of_epochs, 0, -1):
        cur_checkpoint_path = "checkpoints/checkpoint-{}-{}.h5".format(model_id,try_epoch)
        print cur_checkpoint_path
        if os.path.exists(cur_checkpoint_path):
            resume_from_epoch = try_epoch
            checkpoint_path = cur_checkpoint_path
            break"""

    if resume_from_epoch > 0:
        model.load_weights(checkpoint_path)

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=opt,
                  metrics=['accuracy','categorical_crossentropy']
                  )

    print("batchsize is {}".format(batch_size))
    gen = preprocess_data_generator(batch_size)
    training_start_time = time.time()
    #checkpoint_call = keras.callbacks.ModelCheckpoint('checkpoints/checkpoint-' + str(model_id) + '-{epoch}.h5', save_weights_only = True)
    no_of_epochs = state.no_of_epochs
    time_callback = TimeHistory()
    hist = model.fit_generator(
        generator=gen,
        steps_per_epoch=state.training_samples/batch_size,
        epochs=no_of_epochs,
        verbose=2,
        callbacks=[time_callback],
        initial_epoch=resume_from_epoch,
        # use_multiprocessing=True
    )
    training_end_time = time.time()
    sess.close()

    return hist, training_end_time - training_start_time, batch_size, time_callback.times


def main():
    parser = argparse.ArgumentParser(description='help for command')
    parser.add_argument('--model_args_csv_file', type=str,
                        help='no of processes on each node', required=True)
    parser.add_argument('--datapath', type=str,
                        help='path to dataset folder', required=True)
    parser.add_argument('--system', required=True,choices=['lab','summitdev','summit','summitdev_all_gpu_0'])
    parser.add_argument('--gpu_mem', type=int, required=True)
    parser.add_argument('--per_group_dnns_count', type=int, required=True)
    parser.add_argument('--index_of_group', type=int, required=True)
    parser.add_argument('--predict_batchsize', type=int, required=True)
    parser.add_argument('--do_log', type=int, required=True)

    args = parser.parse_args()
    try:
        os.makedirs(args.resource_log_folder)
    except:
            pass
    system_util_logger = None
    if args.do_log:
        system_util_logger = logs.Resource_Util_Logger(30)
    state.datapath = args.datapath
    system_type = args.system
    per_group_dnns_count = args.per_group_dnns_count
    index_of_group = args.index_of_group
    predict_batchsize = args.predict_batchsize

    with open(args.model_args_csv_file, 'rb') as f:
        lines = f.readlines()
        data = [l.split(',') for l in lines[1:]]
        data = data[index_of_group *
                    per_group_dnns_count:(index_of_group+1)*per_group_dnns_count]
        print(len(data))
    assigned_model = []
    gpu_mem = args.gpu_mem
    dnn_index = index_of_group *per_group_dnns_count
    for i in range(rank, len(data), world_size):
        #if os.path.exists("checkpoints/checkpoint-{}-completed.h5".format(i)):
        #    continue
        assigned_model.append((dnn_index+i,
                               data[i][0],
                               [int(t) for t in data[i][1:5]],
                               int(data[i][6]),
                               int(data[i][7]),
                               int(data[i][8])
                               ))
    print "lkddd~    {}_{}".format(rank, assigned_model)

    for dnn_index, model_type, blocks, flops, memory, params in assigned_model:
        start = time.time()
        hist, training_time, batch_size , training_times= train(
            model_type, blocks, gpu_mem, flops, memory, params, system_type,predict_batchsize, dnn_index)
        end = time.time()

        log_str = "\n lk~{}___{}___{}___{}___{}___{}___{}___{}\n".format(dnn_index,
                                                                         [model_type, blocks, flops, memory, params],
                                                                         training_time,
                                                                         batch_size,
                                                                         hist.history,
                                                                         state,
                                                                         predict_batchsize,
                                                                         training_times
                                                                         )
        with open("checkpoints/checkpoint-{}-completed.h5".format(dnn_index),'w') as f:
            f.write(log_str)

        logging.critical(log_str)

    print rank, assigned_model
    if system_util_logger:
        system_util_logger.stop()

main()
