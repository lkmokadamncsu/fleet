from mpi4py import MPI
from multiprocessing import Process
from threading import Thread
import numpy as np
import tensorflow as tf


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
print rank

from multiprocessing import Process, Queue

data_size = 3*244*244*16

flag = 100000

def f(q,q1):
    global flag
    np.random.seed(0)
    data = np.random.random((1,data_size))

    tensorflow_config = tf.ConfigProto(device_count={'GPU': 0}, log_device_placement=True)
    sess = tf.Session(config=tensorflow_config)

    while flag > 0:

        p = q1.get()
        c = tf.constant(data)
        t_data = sess.run(c)
        np.copyto(p,t_data)
        q.put(p)
        flag-=1

def bcast_check():

    q = Queue()
    q1 = Queue()
    p = Process(target=f, args=(q,q1,))
    p.daemon = True
    global flag

    print rank
    #print q.get()    # prints "[42, None, 'hello']"
    if rank ==  0:
        tensorflow_config = tf.ConfigProto()
        tensorflow_config.gpu_options.visible_device_list = str(rank)

        p.start()


        while flag > 0:
            placeholder = np.array([1.50] * data_size)
            #print("placehoder is {}".format(placeholder))
            q1.put(placeholder)
            data = q.get()
            comm.bcast(data,root=0)
            flag-=1

    else :
        np.random.seed(0)
        data_v = np.random.random((1,data_size))
        while flag > 0:
            data = comm.bcast(None, root=0)

            assert (data == data_v).all()
            flag-=1
            print("\n\n\n ko {} \n\n\n\n".format(flag))



if __name__ == '__main__':
    bcast_check()
