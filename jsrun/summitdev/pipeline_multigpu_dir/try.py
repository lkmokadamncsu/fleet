from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
import tensorflow as tf
if rank < 4:
    tensorflow_config = tf.ConfigProto()
    tensorflow_config.gpu_options.visible_device_list = str(rank)
    comm.barrier()
else:
    comm.barrier()
    import os
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"]=""
    tensorflow_config = tf.ConfigProto(device_count={'GPU': 0}, log_device_placement=True)
sess = tf.Session(config=tensorflow_config)
a = tf.constant(5.0)
b = tf.constant(6.0)
c = a * b
print(sess.run(c))
