import argparse
import subprocess, time
import random

jsrun_code = """#!/bin/bash
#BSUB -P csc287
#BSUB -J {}
#BSUB -o {}.o%J
#BSUB -W 360
#BSUB -nnodes {}

module load cuda

source /lustre/atlas2/csc287/proj-shared/lkmokadam/lkmokadam_home/env.sh
cd /lustre/atlas2/csc287/proj-shared/lkmokadam/lkmokadam_home

date
echo $PYTHONPATH


cd /lustre/atlas2/csc287/proj-shared/lkmokadam/workspace/distributed_preprocessing_and_training/

jsrun -n{} -a1 -g1 -c5  python horovod_profile.py --datapath /lustre/atlas2/csc287/proj-shared/lkmokadam/data/objects_256 --gpu_mem=16 --model_args_csv_file Data/model_args.csv --index_from={} --index_to={}



"""
def main():

    parser = argparse.ArgumentParser(description='help for command')

    parser.add_argument('--out', type=str, required=True)
    parser.add_argument('--dnn_from', type=int, default=0)
    parser.add_argument('--dnn_to', type=int, default=100)
    parser.add_argument('--steps', type=int, default=100)

    args = parser.parse_args()
    out = args.out
    dnn_from = args.dnn_from
    dnn_to = args.dnn_to
    steps = args.steps

    with open(out,'w') as of:

        for nnodes in range(1,2):

           for dnn_i in range(dnn_from,dnn_to,steps):
               for n1 in [(nnodes-1)*4+1,(nnodes-1)*4+2,(nnodes-1)*4+3,(nnodes-1)*4+4]:

                   out_str = ""

                   job_name = "{}_{}_{}_{}_auto_2".format(nnodes,dnn_i,dnn_i+steps,n1)

                   code = jsrun_code.format(job_name,job_name,nnodes,n1,dnn_i,dnn_i+steps)
                   filename = "{}.lsf".format(job_name)
                   with open(filename,'w') as f:
                       f.write(code)
                   #time.sleep(random.choice([4,5,6,7]))
                   #out = subprocess.check_output(['bsub', filename])

                   out_str += "======================= \n"
                   out_str+= code+"\n"
                   out_str+= out +"\n"
                   out_str+= "=======================\n"
                   #time.sleep(random.choice([3,4,5,6,7,8]))
                   of.write(out_str)
                   print ".",
main()
