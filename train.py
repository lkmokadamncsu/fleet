import imp
import os
import time
import threading
import random
import logs
import Queue
import sync
import sys
import config
import argparse
import keras
from slim.producer_preprocessing import vgg_preprocessing
import tensorflow as tf
from config.state import state
import csv
from mpi4py import MPI
import horovod.tensorflow as hvd
from trainer import trainer
from multiprocessing import Process
from threading import Thread
#from tensorflow.python.client import device_lib
#print(device_lib.list_local_devices())

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
world_size = comm.Get_size()

print("started at {}".format(rank))

def file_path_and_label_function():
    images = []
    labels = []
    f = open(config.state.state.datapath+'/labels.csv')
    csv_f = csv.reader(f)
    next(csv_f, None)
    for row in csv_f:
        for _ in range(1):
            images.append((config.state.state.datapath+"/"+row[0]).strip())
            labels.append(row[1].strip())
    images, labels = images, labels

    print(" \n\n Images", len(images), " : Labels", len(labels),"\n\n")
    return images, labels


def main():

    parser = argparse.ArgumentParser(description='help for command')

    parser.add_argument('--datapath', type=str,
                        help='path to dataset folder', required=True)
    parser.add_argument('--no_of_parallel_preprocessing_calls', type=int,
                        help='number of cpu cores on each node', required=True)
    parser.add_argument('--mpi_queuing_batch', type=int,
                        help='batching of images', required=True)
    parser.add_argument('--logs_dir', type=str,
                        help='logs directory', required=True)
    parser.add_argument('--preprocessing_data_size', type=int,
                        help='No of images in one preprocessing data message sent over mpi', required=True)

    parser.add_argument('--arch_py_file', type=str,
                        help=' architecture python file', required=True)
    parser.add_argument('--nb_train_samples', type=int,
                        help=' number of training samples', required=True)
    parser.add_argument('--enable_util_logging', type=int,
                        help=' Enable Util logging', required=True)
    args = parser.parse_args()

    # create logs directory

    logs_path = os.path.join(args.logs_dir,"logs")



    try:

        os.makedirs(logs_path)
    except OSError:
        pass

    # import architecture file


    arch = imp.load_source("arch", args.arch_py_file)

    # process arch file
    model_name = None
    trainer_batch_size = -1
    epochs = -1
    training_list_partition = []
    training_leaders_list = []

    # Get training partitions
    for k in arch.training_clusters_dict.keys():

        cluster_ranks = [k] + arch.training_clusters_dict[k]
        training_list_partition.append(cluster_ranks)
        training_leaders_list.append(k)

    model_args = None
    for partition in training_list_partition:
        if rank in partition:
            model_args = arch.cluster_model_assignment_dict[partition[0]]
            trainer_batch_size = arch.cluster_model_assignment_dict[partition[0]]["batch_size"]

    assert(model_args is not None)

    epochs = arch.epochs


    checkpoint_path = completed_model_path = None

#    if model_args:

    checkpoint_path = os.path.join(args.logs_dir,"status","checkpoints",str(model_args["model_id"]))
    print checkpoint_path

    completed_model_path = os.path.join(args.logs_dir,"status","completed_model")

    try:
        os.makedirs(checkpoint_path)
    except OSError:
        pass

    try:
        os.makedirs(completed_model_path)
    except OSError:
        pass

    completed_model_path = os.path.join(args.logs_dir,"status","completed_model",str(model_args["model_id"])+".h5")






    # init multicomm_horovod and state
    if True:
        for partition in training_list_partition:
            if rank in partition:
                hvd.init(partition)
    else:
        hvd.init(training_list_partition, False)

    config.state.state = config.state.State(
        rank=rank,
        world_size=world_size,

        # data from model_args file
        preprocessing_ranks_list=sorted(arch.preprocessing_ranks_list),
        training_clusters_dict=arch.training_clusters_dict,


        datapath=args.datapath,
        no_of_parallel_preprocessing_calls=args.no_of_parallel_preprocessing_calls,
        trainer_batch_size=trainer_batch_size,
        mpi_queuing_batch=args.mpi_queuing_batch,
        preprocessing_data_size = args.preprocessing_data_size,

        # logs and status path
        logs_dir=logs_path,
        checkpoint_path = checkpoint_path,
        completed_model_path = completed_model_path,


        input_shape = arch.input_shape,
        nb_train_samples=args.nb_train_samples,
        model_args=model_args,
        epochs=epochs,
        per_sec_data_limit=arch.per_sec_data_limit/hvd.size()
    )




    print "created state"
    # create comm
    proper_training_list = sorted(arch.preprocessing_ranks_list)+list(set(config.state.state.training_leaders_list)-set(arch.preprocessing_ranks_list))
    group = comm.Get_group()
    trainer_group = MPI.Group.Incl(
        group, sorted(proper_training_list))
    config.state.state.trainer_comm = comm.Create(trainer_group)

    config.state.state.local_trainer_comm = comm.Split_type(
        MPI.COMM_TYPE_SHARED)
    config.state.state.local_trainer_rank = config.state.state.local_trainer_comm.Get_rank()

    system_util_logger = None
    if args.enable_util_logging:
        print "logging started"
        system_util_logger = logs.Resource_Util_Logger(10)

    # Start Logging
    # logs.start_resource_utilization_loging(logs_dir_path=config.state.state.logs_dir, rank=rank)
    proc = None
    if config.state.state.rank in config.state.state.preprocessing_ranks_list:
        f_t = Thread(target = sync.fill_input_queue, name = "fill_input_queue")
        #f_t.daemon = True
        f_t.start()
        proc = Process(target = sync.data_producer, args = (preprocessor_fn, file_path_and_label_function, config.state.state ,))
        #p.daemon = True
        proc.start()
    trainer.trainer()
    config.state.state.logger.info(">>>>>> training completed on %s <<<<<<", config.state.state.rank)

    if config.state.state.local_trainer_comm:
        config.state.state.local_trainer_comm.Free()
    if config.state.state.trainer_comm:
        config.state.state.trainer_comm.Free()

    # creating exit status log string

    config.state.state.logger.critical("\n\nlk~exit_log~{}~{}\n\n".format(config.state.state.log_dict, config.state.state.__dict__))

    print("Joining ",rank)
    config.state.state.shutdown = True
    if proc:

        while not config.state.state.preprocessor_mpi_input_queue.empty():
            print config.state.state.preprocessor_mpi_input_queue.qsize()
            _ = config.state.state.preprocessor_mpi_input_queue.get()
        while not config.state.state.preprocessor_mpi_queue.empty():
            print config.state.state.preprocessor_mpi_input_queue.qsize()
            _ = config.state.state.preprocessor_mpi_queue.get()

        f_t.join()
        proc.join()

    #while not config.state.state.trainer_mpi_queue.empty():
    #    _ = config.state.state.trainer_mpi_queue.get()

    if system_util_logger:
        system_util_logger.stop()
    print("done joining",rank)

def preprocessor_fn(filename, label):
    image_string = tf.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = vgg_preprocessing.preprocess_for_train(image, 224, 224)
    return image, label


if __name__ == '__main__':
    main()
    #config.state.state.shutdown = True
    time.sleep(2)
    for t in threading.enumerate()[1:]:
        time.sleep(1)
        config.state.state.logger.critical("\n rank: {} , Threads: {} \n".format(rank,t.name))


    comm.barrier()
    print("#################################################################")
    print("#################################################################")
    print("#################################################################")
    print("#################################################################")
    print("\n exiting \n")
    print("#################################################################")
    print("#################################################################")
    print("#################################################################")
    #os._exit(0)
