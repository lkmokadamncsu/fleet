import logging.config, os, multiprocessing
from pprint import pprint
import multiprocessing
import Queue
from ratelimiter import RateLimiter

PREPROCESSOR_RANK = 11
CONSUMER_RANK = 12
TRAINER_RANK = 13
UNUSED_RANK = 14


class State:
    def __init__(self,
                 rank,
                 world_size,

                 preprocessing_ranks_list,
                 training_clusters_dict,

                 datapath,
                 trainer_batch_size,
                 mpi_queuing_batch,
                 preprocessing_data_size,
                 no_of_parallel_preprocessing_calls,

                 logs_dir,
                 checkpoint_path,
                 completed_model_path,

                 input_shape,
                 nb_train_samples,
                 model_args,
                 epochs,
                 per_sec_data_limit
                 ):

        logging.config.fileConfig('logging.conf')

        os.system("mkdir -p " + logs_dir)


        self.limiter_rate = per_sec_data_limit
        self.rank = rank
        self.world_size = world_size

        self.preprocessing_ranks_list = preprocessing_ranks_list

        self.training_clusters_dict = training_clusters_dict
        self.training_leaders_list = sorted(training_clusters_dict.keys())
        self.training_followers_list = []
        for v in self.training_clusters_dict.values():
            self.training_followers_list += v

        self.training_ranks_list = self.training_leaders_list + self.training_followers_list

        if rank in self.preprocessing_ranks_list:
            self.logger = logging.getLogger('preprocessor')
        elif rank in self.training_leaders_list:
            self.logger = logging.getLogger('training_leader')
        else:
            self.logger = logging.getLogger('training_followers')

        self.datapath = datapath

        self.no_of_parallel_preprocessing_calls = no_of_parallel_preprocessing_calls
        self.trainer_batch_size = trainer_batch_size
        self.mpi_queuing_batch = mpi_queuing_batch
        self.preprocessing_data_size = preprocessing_data_size
        self.logs_dir = logs_dir

        self.checkpoint_path = checkpoint_path
        self.completed_model_path = completed_model_path

        self.preprocessor_mpi_input_queue = None
        if rank in self.preprocessing_ranks_list:
            self.preprocessor_mpi_queue = multiprocessing.Queue(self.mpi_queuing_batch)
            self.preprocessor_mpi_input_queue = multiprocessing.Queue(self.mpi_queuing_batch)
            self.preprocessor_mpi_queue.cancel_join_thread()
            self.preprocessor_mpi_input_queue.cancel_join_thread()
        self.trainer_mpi_queue = Queue.Queue(self.mpi_queuing_batch)

        self.nb_train_samples = nb_train_samples

        self.input_shape = input_shape
        self.model_args = model_args
        self.epochs = epochs

        for cluster_leader in training_clusters_dict.keys():
            if rank in [cluster_leader] + training_clusters_dict[cluster_leader]:
                self.nb_train_samples /= len([cluster_leader] + training_clusters_dict[cluster_leader])

        self.trainer_comm = None
        self.log_dict = {}
        self.shutdown = False


state = None
