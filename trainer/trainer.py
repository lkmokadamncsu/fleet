# coding: utf-8

from ratelimit import limits
import horovod.keras as hvd
import mpi4py.rc, collections
print(mpi4py.rc.thread_level)
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
import models
from keras.models import Model
import numpy as np
import keras, os, time, random
import threading
from keras.utils import multi_gpu_model
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import keras.backend as K
import config, sys, os, socket
import time,pdb, cPickle
#from multiprocessing import Process
import Queue
#import lkhorovod.tensorflow as lk_hvd

class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)



def images_distribution():

    #print "Recv Message Started"
    img_count = 0
    done_message_counter = 0
    # get rank in trainer_comm
    trainer_rank = -1
    trainer_world_size = -1
    no_of_preprocessors =  len(config.state.state.preprocessing_ranks_list)

    if config.state.state.rank in config.state.state.training_leaders_list:

        trainer_rank = config.state.state.trainer_comm.Get_rank()
        trainer_world_size = config.state.state.trainer_comm.Get_size()

        cluster_receiver_index = 0

        cluster_ranks_of_this_leader = [rank] + config.state.state.training_clusters_dict[rank]

        completed_consumer_rank_index_bitmap = [False] * len(config.state.state.preprocessing_ranks_list)
        completed_consumer_rank_index_count = [0] * len(config.state.state.preprocessing_ranks_list)
        if config.state.state.rank in config.state.state.training_leaders_list:

            while True:
                # print config.state.state.rank, config.state.state.trainer_mpi_queue.qsize()
                for rank_index, global_rank in enumerate(config.state.state.preprocessing_ranks_list):

                    if completed_consumer_rank_index_bitmap[rank_index]:
                        continue

                    if global_rank == config.state.state.rank:
                        data_list = None
                        while data_list is None:

                            try:
                                data_list = config.state.state.preprocessor_mpi_queue.get(timeout = 2)
                            except Queue.Empty:
                                config.state.state.logger.warn("preprocessor queue empty %d %d",rank, done_message_counter)
                                time.sleep(0.1)

                        config.state.state.trainer_comm.Bcast([data_list,MPI.FLOAT],root = rank_index)


                    else:

                        data_list = np.empty((224*224*3+1), dtype=np.float32)
                        config.state.state.trainer_comm.Bcast([data_list, MPI.FLOAT], root = rank_index)

                        assert(data_list is not None)
                        assert(rank_index != trainer_rank)

                    assert(data_list is not None)
                    done_flag = False
                    #print(type(data_list))
                    if (data_list == -1).all():
                        completed_consumer_rank_index_bitmap[rank_index]=True
                        done_flag = True
                        print "==========================================",rank, rank_index
                    else:
                        img_count += 1
                    config.state.state.logger.debug("Received  data at training leader %d %d", config.state.state.rank, data_list is not None)

                    # if received done from consumers, broadcast to all
                    if done_flag:
                        config.state.state.logger.debug("Received  done at training leader %d %d ", config.state.state.rank, data_list is not None)
                        for cluster_receiver in cluster_ranks_of_this_leader:
                            if cluster_receiver == config.state.state.rank:
                                success = False
                                while not success:
                                    try:
                                        config.state.state.trainer_mpi_queue.put("done", timeout = 2)
                                        success = True
                                    except Queue.Full:
                                        config.state.state.logger.warn("Trainer queue Full L104 at %d",rank)
                                        time.sleep(0.1)
                            else:
                                config.state.state.logger.debug("Send done at training leader %d %d", config.state.state.rank, cluster_receiver)
                                comm.Send([data_list, MPI.FLOAT], cluster_receiver, tag = 12)

                    else:
                        #print ">>> adding", rank
                        cluster_receiver  = cluster_ranks_of_this_leader[cluster_receiver_index]
                        if cluster_receiver == config.state.state.rank:
                            #print ">>> adding_1", rank
                            success = False
                            while not success:
                                try:
                                    config.state.state.trainer_mpi_queue.put(data_list, timeout = 2)
                                    success = True
                                except Queue.Full:
                                    config.state.state.logger.warn("Trainer queue is full L119 at %d",rank)
                                    time.sleep(0.1)
                        else:
                            config.state.state.logger.debug("Sending at training leader %d %d %d", config.state.state.rank, cluster_receiver, (data_list == -1).all())
                            comm.Send([data_list, MPI.FLOAT], cluster_receiver, tag = 12)
                        cluster_receiver_index = (cluster_receiver_index + 1) % len(cluster_ranks_of_this_leader)

                    if done_flag:

                        done_message_counter += 1
                        #print ">>>>>>", done_message_counter,"\n"
                        completed_consumer_rank_index_count[rank_index] += 1
                        if done_message_counter == no_of_preprocessors:
                            config.state.state.logger.critical("Stopping receiveing daemon on %s, done message counter = %d, Received Data %d", config.state. state.rank, done_message_counter, img_count)
                            return
    elif config.state.state.rank in config.state.state.training_followers_list:

        done_message_counter = 0
        #print config.state.state.training_followers_list
        while True:
            done_flag = False
            data_list = np.empty((224*224*3+1), dtype=np.float32)
            comm.Recv([data_list, MPI.FLOAT], source=MPI.ANY_SOURCE, tag=12)
            if (data_list == -1).all():
                done_flag = True
            if not done_flag:
                img_count += 1
                config.state.state.logger.debug("Received  data at training followers %d", config.state.state. rank)
                success = False
                while not success:
                    try:
                        config.state.state.trainer_mpi_queue.put(data_list, timeout = 2)
                        success = True
                    except Queue.Full:
                        config.state.state.logger.warn("Trainer queue is full L154 %d",rank)
                        time.sleep(0.1)
            else:
                config.state.state.logger.debug("receiving  done at follower  %d", config.state.state.rank)
                config.state.state.trainer_mpi_queue.put("done")
                done_message_counter += 1
                if done_message_counter == no_of_preprocessors:
                    config.state.state.logger.critical("Stopping receiveing daemon on %s, done message counter = %d, Received Data %d", config.state.state.rank, done_message_counter, img_count)
                    return
    else:

        #print(config.state.state.training_followers_list)
        raise Exception("Invalid type of system {}, {}".format(config.state.state.rank,config.state.state.training_followers_list))
    raise Exception("You should not be here {}, {}".format(config.state.state.rank,config.state.state.training_followers_list))


def data_gen_1():

    @limits(calls=config.state.state.limiter_rate, period=1)
    def process_data_packet(data_packet):

        if isinstance(data_packet, str):
            config.state.state.logger.info("Received done on %s, Received Data %d, Received_images, %d",config.state.state.rank, img_pkg_count, img_count)
            return None, None

        height, width, channel = 224, 224, 3

        image_ = np.array(data_packet_list[:-1])

        label_b = np.array(data_packet_list[-1])
        height, width, channel = 224, 224, 3
        label_ = keras.utils.to_categorical(label_b, num_classes=1000)

        image_ = np.reshape(image_, newshape= (1, height, width, channel))
        label_ = np.reshape(label_, newshape=(1,1000))

        return image_, label_




    trainer_mpi_queue = config.state.state.trainer_mpi_queue
    batch_index = img_pkg_count = img_count = done_message_counter = 0

    image = []
    label = []

    while True:
        if trainer_mpi_queue.empty():
            #config.state.state.logger.warn("trainer queue empty L198 %d",rank)
            time.sleep(0.1)
            continue

        data_packet_list = None
        while data_packet_list is None:
            try:
                data_packet_list = trainer_mpi_queue.get(timeout = 0.1)
            except Queue.Empty:
                config.state.state.logger.warn("trainer queue empty L198 %d",rank)
                time.sleep(1)

        #data_packet_list = cPickle.loads(data_packet_list)
        if data_packet_list is None:
            config.state.state.logger.warn("Received None Data at rank {}".format(config.state.state.rank))
            continue
        img_pkg_count+=1
        data_packet = data_packet_list

        image_, label_ = process_data_packet(data_packet)
        if image_ is None:
            done_message_counter += 1
            if done_message_counter == len(config.state.state.preprocessing_ranks_list):
                config.state.state.logger.warn("Done Preprocessor at rank {}".format(config.state.state.rank))
                #print "done preprocessor"
                return
            continue
        assert(image_ is not None)
        #print img_count
        img_count+=1
        if batch_index < config.state.state.trainer_batch_size:
            if batch_index == 0:
                image = image_
                label = label_
            else:
                image = np.append(image,image_,axis=0)
                label = np.append(label,label_, axis=0)
            batch_index+=1
            continue
        else:
            config.state.state.logger.debug(" Training queue size at rank %d : %d",config.state.state.rank,trainer_mpi_queue.qsize())
            batch_index = 0
            random_indexes = np.arange(image.shape[0])
            np.random.shuffle(random_indexes)
            #print image, label
            yield image[random_indexes],label[random_indexes]
            # make sure current generate will send current in next batch
            image = image_
            label = label_
            batch_index+=1
            img_pkg_count+=1
            #print ">>",img_pkg_count




def dist_vgg():

    # added due to multiprocessing
    #config.state.state = config_state
    #os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
    #os.environ["CUDA_VISIBLE_DEVICES"]="0,1,2,3"

    epochs = config.state.state.epochs


    #print("{}  {}  {} ".format(">>lkma", rank, socket.gethostname()))

    keras_config = tf.ConfigProto()
    keras_config.intra_op_parallelism_threads =  20
    keras_config.inter_op_parallelism_threads = 20
    #keras_config.gpu_options.visible_device_list = str(rank)
    keras_config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    #keras_config.log_device_placement = True  # to log device placement (on which device the operation ran)
    tf.reset_default_graph()
    sess = tf.Session(config=keras_config)
    set_session(sess)  # set this TensorFlow session as the default session for Keras
    #K.set_session(K.tf.Session(config=K.tf.ConfigProto(intra_op_parallelism_threads=20, inter_op_parallelism_threads=20)))

    if config.state.state.trainer_batch_size == -1:
        profile_model = models.get_model(config.state.state.model_args,True)
        flops, config.state.state.trainer_batch_size = get_model_flops_and_memory_usage(model=profile_model, gpu_mem_in_gb=11)

    # If set > 0, will resume training from a given checkpoint.

    resume_from_epoch = 0
    for try_epoch in range(epochs, 0, -1):
        cur_checkpoint_path = os.path.join(config.state.state.checkpoint_path,'checkpoint-{}.h5'.format(try_epoch))
        #print cur_checkpoint_path
        if os.path.exists(cur_checkpoint_path):
            resume_from_epoch = try_epoch
            break


    # Horovod: broadcast resume_from_epoch from rank 0 (which will have
    # checkpoints) to other ranks.
    #resume_from_epoch = hvd.broadcast(resume_from_epoch, 0, name='resume_from_epoch')
    #print config.state.state.log_dict
    #print "Model : {}, Resuming from {} ".format(config.state.state.model_args["model_id"],resume_from_epoch)

    basemodel = models.get_model(config.state.state.model_args)
    last = basemodel.output
    model = Model(basemodel.input, last)

    # Horovod: adjust learning rate based on number of GPUs.
    opt = keras.optimizers.Adadelta(1.0 * hvd.size())

    # Horovod: add Horovod Distributed Optimizer.
    opt = hvd.DistributedOptimizer(opt)

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=opt,
                  metrics=['accuracy','categorical_crossentropy'])
    if resume_from_epoch > 0 and hvd.rank() == 0:
        checkpoint_path =  os.path.join(config.state.state.checkpoint_path,'checkpoint-{}.h5'.format(resume_from_epoch))
        model.load_weights(checkpoint_path)

    callbacks = [
        # Horovod: broadcast initial variable states from rank 0 to all other processes.
        # This is necessary to ensure consistent initialization of all workers when
        # training is started with random weights or restored from a checkpoint.
        hvd.callbacks.BroadcastGlobalVariablesCallback(0),

        # Horovod: average metrics among workers at the end of every epoch.
        #
        # Note: This callback must be in the list before the ReduceLROnPlateau,
        # TensorBoard, or other metrics-based callbacks.
        hvd.callbacks.MetricAverageCallback(),

        # Horovod: using `lr = 1.0 * hvd.size()` from the very beginning leads to worse final
        # accuracy. Scale the learning rate `lr = 1.0` ---> `lr = 1.0 * hvd.size()` during
        # the first five epochs. See https://arxiv.org/abs/1706.02677 for details.
        hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=5, verbose=1),

        # Horovod: after the warmup reduce learning rate by 10 on the 30th, 60th and 80th epochs.
        hvd.callbacks.LearningRateScheduleCallback(start_epoch=5, end_epoch=30, multiplier=1.),
        hvd.callbacks.LearningRateScheduleCallback(start_epoch=30, end_epoch=60, multiplier=1e-1),
        hvd.callbacks.LearningRateScheduleCallback(start_epoch=60, end_epoch=80, multiplier=1e-2),
        hvd.callbacks.LearningRateScheduleCallback(start_epoch=80, multiplier=1e-3),
    ]

    # Horovod: save checkpoints only on the first worker to prevent other workers from corrupting them.
    if hvd.rank() == 0:
        pass
        #callbacks.append(keras.callbacks.ModelCheckpoint(os.path.join(config.state.state.checkpoint_path,'checkpoint-{epoch}.h5')))


    nb_train_samples = config.state.state.nb_train_samples
    batch_size = config.state.state.trainer_batch_size
    #generator = data_gen_1()
    #print("Started Waiting ", rank)
    comm.barrier()
    #print("Done Waiting ", rank)
    config.state.state.logger.info(
        "Starting Training")
    starttime = time.time()
    hist = None



    try:
        hist = model.fit_generator(
            data_gen_1(),
            callbacks=callbacks,
            steps_per_epoch = (nb_train_samples // batch_size),
            epochs=epochs,
            #workers=4,
            #max_q_size=64,
            initial_epoch=0,#resume_from_epoch,
            verbose=2,
            #use_multiprocessing=True
        )
    except StopIteration:
        config.state.state.logger.info("Stopped iteration before receiveing all data. This may be due to incorrect number of data sample"+str(rank))
    #except Exception as e:
    #    print(e)

    finally:
        config.state.state.logger.info(
            "Stopped Training -  %d ",rank)
        if hvd.rank() == 0:
            pass
            #model.save(config.state.state.completed_model_path)
        pass

    endtime = time.time()
    config.state.state.log_dict["type"]="training"
    config.state.state.log_dict["dnn_time"] = endtime - starttime
    config.state.state.log_dict["model_args"] = config.state.state.model_args
    config.state.state.log_dict["cluster_size"] = hvd.size()
    config.state.state.log_dict["hist"] = hist.history if hist else None
    config.state.state.logger.info("Model Saved : {}".format(os.path.join(config.state.state.logs_dir,'vgg_model.h5')))
    sess.close()



def get_model_flops_and_memory_usage(gpu_mem_in_gb, model):
    run_meta = tf.RunMetadata()
    float_operations = tf.profiler.ProfileOptionBuilder.float_operation()
    flops = tf.profiler.profile(graph=K.get_session().graph,
                                run_meta=run_meta, cmd='op', options=float_operations)

    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

    number_size = 4.0
    if K.floatx() == 'float16':
         number_size = 2.0
    if K.floatx() == 'float64':
         number_size = 8.0

    model_memory  = number_size*(shapes_mem_count + trainable_count + non_trainable_count)

    optimum_batch_size = int(((gpu_mem_in_gb*(1024**3))/model_memory))

    config.state.state.log_dict["Model : trainable_count"] = trainable_count
    config.state.state.log_dict["Model : non_trainable_count"] = non_trainable_count
    config.state.state.log_dict["Model : model_memory"] = model_memory
    #print flops.total_float_ops, optimum_batch_size
    return flops.total_float_ops, optimum_batch_size



def trainer():
    #lk_hvd.init(config.state.state.training_leaders_list)

    p = threading.Thread(target=images_distribution)
    p.daemon = True
    p.start()

    dist_vgg()

    while p.is_alive():
        try:
            _ =  config.state.state.trainer_mpi_queue.get(timeout = 2)
        except Queue.Empty:
            config.state.state.logger.warn("trainer queue empty L198 %d",rank)
            time.sleep(1)
    #comm.barrier()
    #p.join()
    config.state.state.logger.info("rank %d : trainer Done", config.state.state.rank)

    #lk_hvd.shutdown()
