#!/usr/bin/env python

import pprint

import pandas as pd

def get_code(preprocessing_ranks_list, consumer_ranks_list, training_clusters_dict, cluster_model_assignment_dict):

    code_str = """
    input_shape = (224, 224, 3)
    epochs = 100


    preprocessing_ranks_list = {}
    consumer_ranks_list = {}
    training_clusters_dict = {}
    cluster_model_assignment_dict = dict()


    {}

    """
    preprocessing_ranks_list = pprint.pformat(
        preprocessing_ranks_list, indent=4)
    consumer_ranks_list = pprint.pformat(consumer_ranks_list, indent=4)
    training_clusters_dict = pprint.pformat(training_clusters_dict, indent=4)
    cluster_model_assignment_dict = pprint.pformat(
        cluster_model_assignment_dict, indent=4)

    generated_code_str = code_str.format(preprocessing_ranks_list,
                                         consumer_ranks_list,
                                         training_clusters_dict,
                                         cluster_model_assignment_dict
                                         )
    return generated_code_str


def generate_matrix(dataset_file):


    data = pd.read_csv(dataset_file)
    data
    model_data = dict()
    for model_id in range(80):
        cur_model_data = []
        for number_of_ranks in range(1, 9):
            loc = data.loc[(data['model_id'] == model_id) & (
                data['number_of_ranks'] == number_of_ranks)]['consumption_rate_per_rank']
            if len(loc) == 0:
                continue
            con_rate = loc.values[0]
            model_data[(model_id, number_of_ranks)] = con_rate*number_of_ranks

    return model_data


def get_stages_list(model_data):

    candidates_model_ids = set(range(80))
    stages = []

    iter = 10
    while len(candidates_model_ids) > 0:
        print iter
        number_of_gpus_available = 100
        cur_stage = []

        cur_stage_refernce_model_data = []
        for t in candidates_model_ids:
            for number_of_ranks in range(1, 9):
                cur_stage_refernce_model_data.append(
                    ((t, number_of_ranks), model_data[(t, number_of_ranks)]))

        point_of_reference_model_key, point_of_reference_model_con_rate = max(
            cur_stage_refernce_model_data, key=lambda x: x[1]/x[0][1])

        cur_stage.append((point_of_reference_model_key,
                          point_of_reference_model_con_rate))
        candidates_model_ids.remove(point_of_reference_model_key[0])
        number_of_gpus_available -= 1

        sum_of_loss = 0
        while number_of_gpus_available > 0:
            cur_best_cand, cur_best_loss = None, 9999

            for model_id in candidates_model_ids:
                for number_of_ranks in range(1, 9):
                    key = (model_id, number_of_ranks)
                    cur_con_rate = model_data[key]

                    if iter == -1:
                        print ">", cur_con_rate, point_of_reference_model_con_rate, cur_best_cand, cur_best_loss
                    cur_loss = abs(
                        point_of_reference_model_con_rate - cur_con_rate)

                    if cur_loss < cur_best_loss:
                        cur_best_loss = cur_loss
                        cur_best_cand = key
            if not cur_best_cand:
                break
            if (number_of_gpus_available - cur_best_cand[1]) < 0:
                break
            cur_stage.append((cur_best_cand, model_data[cur_best_cand]))
            #print candidates_model_ids
            candidates_model_ids.remove(cur_best_cand[0])
            number_of_gpus_available -= cur_best_cand[1]

            #print number_of_gpus_available, more_dnn_possible , cur_best_loss, cur_best_cand, model_data[cur_best_cand], point_of_reference_model_key,point_of_reference_model_con_rate
        stages.append([cur_stage, number_of_gpus_available])
        iter -= 1
        print ">>", len(candidates_model_ids), number_of_gpus_available
        if not iter:
            break

    print point_of_reference_model_key, point_of_reference_model_con_rate
    print candidates_model_ids

    print "\n\n\nStaging without adjustment"
    num = 0
    for s, g in stages:
        print "stage ", num
        print "Remaining GPUs", g
        print(pprint.pformat(sorted(s, key=lambda x: x[-1])))
        num += 1
        print "\n\n"

    for s in stages:
        while s[1] != 0:
            s[0] = sorted(s[0], key=lambda x: x[-1])

            stage_entry = s[0].pop(0)

            slowest_model_id, slowest_model_ranks = stage_entry[0]
            ratio = model_data[(slowest_model_id, 8)] / \
                model_data[(slowest_model_id, 7)]

            k = (slowest_model_id, slowest_model_ranks+1)
            val = model_data[k] if k in model_data else int(
                stage_entry[1]*ratio)
            s[0].append([(slowest_model_id, slowest_model_ranks+1), val])
            s[1] -= 1
            s[0] = sorted(s[0], key=lambda x: x[-1])

    print "\n\n\nStaging after adjustment"
    num = 0
    for s, g in stages:
        print "stage ", num
        print "Remaining GPUs", g
        print(pprint.pformat(sorted(s, key=lambda x: x[-1])))
        num += 1
        print "\n\n"




def main():
    dataset_file = "Data/MultiGpu_Data.csv"
    model_data = generate_matrix(dataset_file=dataset_file)
    print "Hi"
    stages = get_stages_list(model_data)


if __name__ == "__main__":
    main()
