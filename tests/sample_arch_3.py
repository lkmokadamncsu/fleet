"""
--> provide this file as command line argument

7 nodes - 4 processes each - less cpu intensive DNN
preprocessing_ranks_list = [0,4,8]
consumer_ranks_list = [1,5,9]
training_clusters_dict = { 1:[-1], 5:[9], 13:[-1], 17 :[20,24]}


8 nodes - 4 processes each  - for high cpu intensive DNN
one preprocessing, other training
preprocessing_ranks_list = [0,1,2,3]
consumer_ranks_list = [4,8,16]
training_clusters_dict = { 4:[-1], 8:[12], 16:[-1], 20 :[24,28]}

Can have hybrid  too


--> Randall's best framework was All Shared -> can be implemented just changing conf file
add one process from all nodes to preprocessing list
"""

input_shape = (224,224,3)
epochs = 1


###########################################################################################
# find the optimal partitioning
# Generate this file for each stage
# run individual mpirun for each stage


preprocessing_ranks_list = [7,8]
consumer_ranks_list = [0,2]
training_clusters_dict = { 0:[1], 2:[3,4] , 5:[6] }


cluster_model_assignment_dict = {}

cluster_model_assignment_dict[0] = {"model_name": "resnet",
                                    "batch_size" : -1,
                                    "stack_args": [3,4,6,3],
                                    "input_shape": input_shape}
cluster_model_assignment_dict[2] = {"model_name": "resnet",
                                    "batch_size" : -1,
                                    "stack_args": [3,4,23,3],
                                    "input_shape": input_shape}
cluster_model_assignment_dict[5] = {"model_name": "resnet",
                                    "batch_size" : -1,
                                    "stack_args": [3,8,36,3],
                                    "input_shape": input_shape}




log_dir_ext = "MobileNet"

