"""
--> provide this file as command line argument

7 nodes - 4 processes each - less cpu intensive DNN
preprocessing_ranks_list = [0,4,8]
consumer_ranks_list = [1,5,9]
training_clusters_dict = { 1:[-1], 5:[9], 13:[-1], 17 :[20,24]}


8 nodes - 4 processes each  - for high cpu intensive DNN
one preprocessing, other training
preprocessing_ranks_list = [0,1,2,3]
consumer_ranks_list = [4,8,16]
training_clusters_dict = { 4:[-1], 8:[12], 16:[-1], 20 :[24,28]}

Can have hybrid  too


--> Randall's best framework was All Shared -> can be implemented just changing conf file
add one process from all nodes to preprocessing list
"""

input_shape = (224, 224, 3)
epochs = 1


###########################################################################################
# find the optimal partitioning
# Generate this file for each stage
# run individual mpirun for each stage


preprocessing_ranks_list = [2]
consumer_ranks_list = [0]
training_clusters_dict = {0: [1]}


# change turn to test
turn = 1

cluster_model_assignment_dict = {}
if turn == 0:

    cluster_model_assignment_dict[0] = {"model_name": "mobilenet",
                                        "batch_size": -1,
                                        "width_multiplier": 0.5,
                                        "input_shape": input_shape,
                                        "classes": 1000
                                        }

    cluster_model_assignment_dict[2] = {"model_name": "mobilenet",
                                        "batch_size": -1,
                                        "width_multiplier": 0.75,
                                        "input_shape": input_shape,
                                        "classes": 1000
                                        }

    cluster_model_assignment_dict[5] = {"model_name": "mobilenet",
                                        "batch_size": -1,
                                        "width_multiplier": 1,
                                        "input_shape": input_shape,
                                        "classes": 1000
                                        }


elif turn == 1:
    batch_size = 32

    cluster_model_assignment_dict[0] = {"model_name": "densenet",
                                        "batch_size": batch_size,
                                        "blocks": [6, 12, 32, 32],
                                        "input_shape": input_shape,
                                        "classes": 1000}

    cluster_model_assignment_dict[2] = {"model_name": "densenet",
                                        "batch_size": batch_size,
                                        "blocks": [6, 12, 32, 32],
                                        "input_shape": input_shape,
                                        "classes": 1000}

    cluster_model_assignment_dict[5] = {"model_name": "densenet",
                                        "batch_size": batch_size,
                                        "blocks": [6, 12, 32, 32],
                                        "input_shape": input_shape,
                                        "classes": 1000}

elif turn == 2:

    cluster_model_assignment_dict[0] = {"model_name": "resnet",
                                        "batch_size": -1,
                                        "stack_args": [3, 4, 6, 3],
                                        "input_shape": input_shape,
                                        "classes": 1000}

    cluster_model_assignment_dict[2] = {"model_name": "resnet",
                                        "batch_size": -1,
                                        "stack_args": [3, 6, 4, 3],
                                        "input_shape": input_shape,
                                        "classes": 1000}

    cluster_model_assignment_dict[5] = {"model_name": "resnet",
                                        "batch_size": -1,
                                        "stack_args": [3, 5, 5, 3],
                                        "input_shape": input_shape,
                                        "classes": 1000}


else:
    raise Exception(" invalid turn value ")


log_dir_ext = "MobileNet"
